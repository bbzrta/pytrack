from SpotifyScraper.scraper import Scraper, Request
import re


request = Request().request()
scraper = Scraper(session=request)
playlist_information = scraper.get_playlist_url_info(url=input("Enter the Spotify URL: "))
password_list = []
requested_info = ["track_name", "track_singer", "track_album"]
leet = {
    "a": "4",
    "i": "1",
    "e": "3",
    "t": "7",
    "o": "0",
    "s": "5",
    "g": "9",
    "z": "2",
}

# Iterate over all pairs of dictionary by index and
# print the pair at index n
for index, (key, value) in enumerate(playlist_information.items()):
    if index == 5:
        for i in range(len(value)):
            for micro_index, (micro_key, micro_value) in enumerate(value[i].items()):

                delete_value = re.findall(r'\(.*?\)', str(micro_value))
                for y in delete_value:
                    if y in micro_value:
                        micro_value = micro_value.replace(y, "")

                for x in requested_info:
                    if micro_key == x and micro_value not in password_list:
                        password_list.append(micro_value)
                        password_list.append(micro_value.lower())
                        password_list.append(micro_value.title())
                        password_list.append(micro_value.upper())
                        for lkey, lvalue in leet.items():
                            if micro_value.replace(lkey, lvalue) not in password_list:
                                password_list.append(micro_value.replace(lkey, lvalue))


outfile = open(input("enter the file name: "), 'a')

for i in password_list:
    word = i.replace(" ", "")
    outfile.write(word + "\n")
