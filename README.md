# PyTrack

A simple project to generate wordlist out of Spotify playlists.

## Getting Started

To get started clone the project onto your home directory by running:

```
git clone https://gitlab.com/bbzrta/pytrack.git
```

### Prerequisites

What things you need to install the software and how to install them

```
$ pip install -U spotifyscraper
```

### Installing

do not need to install just cd into script directory and run

```
python start.py
```

### Things that would be nice to implement into the project

```
- Tweet Scraping
- leet(l33t) rules - DONE
- removing bracket contents - DONE
```
